/*
Create & destroy AWS EC2 instance backups.
*/
package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

type argv struct {
	create  *bool
	destroy *bool
	dryRun  *bool
	reboot  *bool
	list    *bool

	instanceId         *string
	prefix             *string
	lifetime           *string
	credentialsCSV     *string
	credentialsProfile *string
	region             *string
	date               *string
	count              *int
}

type responce struct {
	CreateImageOutput    *ec2.CreateImageOutput
	CreateTagsOutput     *ec2.CreateTagsOutput
	DescribeImagesOutput *ec2.DescribeImagesOutput
	ListInstancesOutput  *[]string
}

func main() {
	arg := &argv{
		create:             flag.Bool("create", false, "Create volume snapshot & AMI from my instance."),
		destroy:            flag.Bool("destroy", false, "Destroy old AMI & volume snapshots."),
		dryRun:             flag.Bool("dryRun", false, "Dry run request."),
		reboot:             flag.Bool("reboot", false, "Create [in]consistent snapshot(s) with[out] reboot."),
		list:               flag.Bool("list", false, "Get list of instaces."),
		instanceId:         flag.String("instanceId", "", "Instance ID from to make AMI"),
		prefix:             flag.String("prefix", "snap-foo", "Backed up AMI prefix."),
		lifetime:           flag.String("lifetime", "", "AMI lifetime X[d]days, X[h]hours, X[m]minutes"),
		credentialsCSV:     flag.String("credentialsCSV", "/foo/.aws/credentials", "Path to cred."),
		credentialsProfile: flag.String("credentialsProfile", "foo", "Profile name"),
		region:             flag.String("region", "eu-central-1", "AWS region."),
		date:               aws.String("2006-01-02 15_04_05 MST"),
		count:              flag.Int("count", 0, "Minimum available AMI's after destroy operationi. Overrides --lifetime"),
	}

	flag.Parse()
	err := argValidate(arg)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	client, err := ec2client(arg)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	resp := new(responce)
	switch {
	case *arg.list:
		resp.ListInstancesOutput, err = listInstances(client)
		if err != nil {
			fmt.Fprintf(os.Stderr, "", err)
			os.Exit(1)
		}
		for _, id := range *resp.ListInstancesOutput {
			fmt.Println(id)
		}
	case *arg.create:
		resp.CreateImageOutput, err = createImage(client, arg)
		if err != nil {
			fmt.Fprintf(os.Stderr, "", err)
			os.Exit(1)
		}

		resp.CreateTagsOutput, err = tagImage(client, arg, resp)
		if err != nil {
			fmt.Fprintf(os.Stderr, "", err)
			os.Exit(1)
		}

		resp.DescribeImagesOutput, err = describeImage(client, arg, resp)
		switch {
		case err != nil:
			fmt.Fprintf(os.Stderr, "could not get any image output", err)
		case len(resp.DescribeImagesOutput.Images) == 0:
			fmt.Fprintf(os.Stderr, "could not find the AMI just created\n")
		case *resp.DescribeImagesOutput.Images[0].State == "failed":
			fmt.Fprintf(os.Stderr, "created AMI entered a state of 'failed'\n")
		default:
			fmt.Println("created AMI: " + *resp.CreateImageOutput.ImageId)
		}
	case *arg.destroy:
		resp.DescribeImagesOutput, err = instanceImages(client, arg)
		lifetime, err := lifetimeToSeconds(*arg)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			os.Exit(1)
		}
		if len(resp.DescribeImagesOutput.Images) == 0 {
			fmt.Fprintf(os.Stderr, "nothing found to delete\n")
			os.Exit(1)
		}
		for i := 0; i < len(resp.DescribeImagesOutput.Images)-*arg.count; i++ {
			creation, err := time.Parse(time.RFC3339Nano, *resp.DescribeImagesOutput.Images[i].CreationDate)
			if err != nil {
				os.Exit(1)
			}
			if time.Now().Unix()-creation.Unix() > lifetime {
				err := destroyImage(client, arg, resp.DescribeImagesOutput.Images[i])
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
			}
		}
		fmt.Println("Images older than " + *arg.lifetime + " destroyed & " + strconv.Itoa(*arg.count) + " deferred")
	default:
		fmt.Println("==> " + os.Args[0] + " --help")
	}
	os.Exit(0)
}

func argValidate(a *argv) error {
	if *a.create && *a.destroy {
		return errors.New("logic error - create and destroy simultaneously")
	}
	if _, err := os.Stat(*a.credentialsCSV); err != nil {
		return err
	}
	if *a.lifetime == "" && *a.destroy {
		return errors.New("lifetime variable not set")
	}
	if *a.count < 0 {
		return errors.New("logic error - negative counter")
	}
	return nil
}

func lifetimeToSeconds(a argv) (int64, error) {
	var lifetime string = *a.lifetime
	seconds, err := strconv.ParseInt(lifetime[0:len(lifetime)-1], 10, 64)

	re := regexp.MustCompile("(d|h|m)")
	s := re.FindString(lifetime)
	switch s {
	case "d":
		seconds *= 86400
	case "h":
		seconds *= 3600
	case "m":
		seconds *= 60
	default:
		err = fmt.Errorf("expected --lifetime xx[d|h|m] but found " + lifetime)
	}
	return seconds, err
}

func ec2client(a *argv) (*ec2.EC2, error) {
	creds := credentials.NewSharedCredentials(*a.credentialsCSV, *a.credentialsProfile)
	_, err := creds.Get()

	return ec2.New(session.New(), &aws.Config{
		Region:      a.region,
		Credentials: creds,
	}), err
}

func destroyImage(client *ec2.EC2, a *argv, image *ec2.Image) error {
	_, err := client.DeregisterImage(&ec2.DeregisterImageInput{
		DryRun:  a.dryRun,
		ImageId: image.ImageId,
	})
	if err != nil {
		return err
	}
	for i := range image.BlockDeviceMappings {
		_, err := client.DeleteSnapshot(&ec2.DeleteSnapshotInput{
			DryRun:     a.dryRun,
			SnapshotId: image.BlockDeviceMappings[i].Ebs.SnapshotId,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func createImage(client *ec2.EC2, a *argv) (*ec2.CreateImageOutput, error) {
	return client.CreateImage(&ec2.CreateImageInput{
		InstanceId: a.instanceId,
		Name:       aws.String(*a.prefix + " " + *a.instanceId + " " + time.Now().Format(*a.date)),
		DryRun:     a.dryRun,
		NoReboot:   a.reboot,
	})
}

func tagImage(client *ec2.EC2, a *argv, r *responce) (*ec2.CreateTagsOutput, error) {
	return client.CreateTags(&ec2.CreateTagsInput{
		Resources: []*string{r.CreateImageOutput.ImageId},
		Tags: []*ec2.Tag{
			&ec2.Tag{
				Key:   a.prefix,
				Value: a.instanceId,
			},
		},
	})
}

func describeImage(client *ec2.EC2, a *argv, r *responce) (*ec2.DescribeImagesOutput, error) {
	return client.DescribeImages(&ec2.DescribeImagesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("image-id"),
				Values: []*string{r.CreateImageOutput.ImageId},
			},
		},
	})
}

func instanceImages(client *ec2.EC2, a *argv) (*ec2.DescribeImagesOutput, error) {
	return client.DescribeImages(&ec2.DescribeImagesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("tag:" + *a.prefix),
				Values: []*string{a.instanceId},
			},
		},
	})
}

func listInstances(client *ec2.EC2) (*[]string, error) {
	resp, err := client.DescribeInstances(nil)
	if err != nil {
		return nil, err
	}
	instanceIds := []string{}
	for idx, _ := range resp.Reservations {
		for _, inst := range resp.Reservations[idx].Instances {
			instanceIds = append(instanceIds, *inst.InstanceId)
		}
	}
	return &instanceIds, nil
}
