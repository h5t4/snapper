# snapper
Create &amp; destroy AWS EC2 instance backups.

Example:</br>
$ go build snapper.go</br>
$ snapper --help</br>
$ snapper --credentialsCSV "/home/foo/.aws/credentials" --credentialsProfile 'default' --region eu-central-1 --list</br>
$ snapper  --credentialsCSV "/home/foo/.aws/credentials" --credentialsProfile 'default' --region eu-central-1 --create --instanceId i-8888</br>
$ snapper  --credentialsCSV "/home/foo/.aws/credentials" --credentialsProfile 'default' --region eu-central-1 --destroy --count 0 --lifetime 12h --instanceId i-8888</br>

Crontab example:</br>
0 9-17 * * 1-5 /path/to/snapper  --credentialsCSV "/home/foo/.aws/credentials" --credentialsProfile 'default' --region eu-central-1 --prefix hourly --create --instanceId i-8888</br>
5 * * * * /path/to/snapper --credentialsCSV "/home/foo/.aws/credentials" --credentialsProfile 'default' --region eu-central-1 --prefix hourly --destroy --count 0 --lifetime 1d --instanceId i-8888</br>

